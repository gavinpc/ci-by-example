# Publishing an NPM package

This example illustrates how to publish an NPM package to the [Gitlab package
registry](https://docs.gitlab.com/ee/user/packages/npm_registry/).  This is
almost identical to the process for publishing to NPM.

## Note

In order to publish a packages with a scoped name (such as
`@myscope/mypackage`), Gitlab requires the top-level project or group name to
match the scope name.  See the Gitlab issues [Lift the NPM package naming
convention for the project level
API](https://gitlab.com/gitlab-org/gitlab/-/issues/33685) and [Make the npm
project-level endpoint work with all npm
commands](https://gitlab.com/gitlab-org/gitlab/-/issues/220985) for more
information.

## Prerequisites

This technique depends on two access tokens, which are stored in environment
variables:

-  `NPM_PACKAGE_REGISTRY_REPO_TOKEN` stores a token with `write_registry`
  permission on the project.  This token is used to authorize a push to the
  repository itself (for reasons noted below).

-  `NPM_PACKAGE_REGISTRY_PUBLISH_TOKEN` stores a token with the `read_registry`
  and `write_registry` permissions on the project.  This token is used to
  authorize a publish to the project's NPM registry.
  
  > Note: I don't know that both of the permissions are needed (and in fact
  > haven't gotten this to work at all yet).

The variables can use any names, as long as the references in the CI code match.

## Bumping the version number

When you publish a new version, you have to increment its version number.
Bumping the version number is the only way to make sure that package users can
get access to a new version.

But since the `package.json` manifest has the version number baked in, and since
that file is checked into the repository, changing it during the CI means that
the CI must essentially create a new commit.

This example shows how to automate that process.  We don't want to have to think
about the version number every time we make any kind of change.

- bump the version number

- add a commit to repository with the updated version number
