function log(name: string, thing: any): void {
	console.log(`${name} saw a ${thing}`);
}

log("Alice", "UFO");
log("Bob", "sight");
