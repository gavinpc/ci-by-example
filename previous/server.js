const express = require('express');

const app = express();

const {
  PORT = "8899"
} = process.env;

const HOST = "0.0.0.0";

app.use(express.static("site"));

app.use("/static", express.static("site/static", { fallthrough: false }));

app.use("*", (req, res) => {
  res.sendFile("site/index.html", { root: __dirname });
});

app.listen(PORT, HOST);
console.log(`web server listening on port ${HOST}:${PORT}`);
