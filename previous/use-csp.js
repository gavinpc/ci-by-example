const csp = require("js-csp");

const important_events = csp.chan(10);
// Alice is watching
csp.go(function * () {
	const thing = yield csp.take(important_events);
	console.log("Alice saw a " + thing);
});
// Bob is watching
csp.go(function * () {
	const thing = yield csp.take(important_events);
	console.log("Bob saw a " + thing);
});
csp.putAsync(important_events, "solar eclipse");
