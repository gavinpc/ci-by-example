# Custom build runner

This example illustrates the usage of a custom build runner defined within the
project.

## Overview

The end-to-end process of building a software package usually entails the
installation of dependencies.  Based on experience, we can make two observations
about this:

- it takes a significant amount of time

- the same dependencies are used over multiple builds

We would like to pay for the installation of dependencies once and use it over
the course of multiple builds.

These dependencies take the form of files that need to be present in the system
before certain build steps can proceed.

One way to approach this is to use a
[cache](https://docs.gitlab.com/ee/ci/caching/).  We have done this in the past,
and, in short, it doesn't quite solve the problem.  Even with a cache, the
package manager must still go through the work of assesing the dependencies,
which usually involves contacting the registry.  Since the cache may have been
cleared, you still have to run the install step.

Another approach creates a custom runner image with the dependencies
pre-installed.  This way, you can avoid the install step altogether at build
time.

The approach illustrated here creates a build runner and makes it available for
use by other jobs in the pipeline.

The basic technique involves:

- defining the desired a runner image as a Docker container

- building the image when its description is updated

- pushing the image to the project's container registry

- using the (latest) runner image from jobs that need it

The objectives of this pattern are:

- the custom build runner is automatically built and pushed when (and only when)
  any of its inputs change
  
- jobs using the custom runner automatically use the most current build runner

## Usage notes

See the [.gitlab-ci.yml](.gitlab-ci.yml) for the implementation.

### Tracking the latest image

Each time an image is updated, it must get a new identity.  Using a new
identifier for each new image is the only way to ensure that a newer image will
be always be used when it is available.

To track the latest image, we:

- use the pipeline ID of the builder job to tag the image

- store the tag in a designated CI variable

- reference that variable in the `image` of jobs that need it.

> **N.B.**: Don't waste your time with the `:latest` tag.  This stuff is here
> for a reason.

### Forcing a rebuild

Sometimes you want to rebuild the container even if the inputs haven't changed.
(This happens most often when you're working on the CI itself.)

To force a rebuild of the container, you can delete the runner tag variable
associated with the image and re-run the job.

### Target registry

This example publishes to the Gitlab project's container registry, which can be
done using available information from the environment.  With a login to other
registries (such as Docker Hub), an almost identical process can be used.

## Caveats

This does not take branching into account.  All jobs use the same runner,
regardless of what branch they are on.

So, for example, if the runner is modified and rebuilt because of a change on
`master`, jobs on other branches will use the new runner even if they are behind
`master`.

This could theoretically be mitigated by, for example, incorporating branch
names into the image's tag.  However, I haven't tried this as it hasn't yet
become a problem.
