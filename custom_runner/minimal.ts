import * as tx from "@thi.ng/transducers"

const NUMBERS = [42, 19, 2020]
const square = (x: number) => x*x

console.log("hello from TypeScript " + [...tx.map(square, NUMBERS)].join(", "));
