# GitLab CI by example

This repository contains several working examples of Gitlab CI configurations
for various kinds of tasks.

[[_TOC_]]

## Motivation

Every non-trivial project involves some kind of CI automation.  As projects grow
more complex and various, so do their configurations.

[Gitlab's CI/CD facilities](https://docs.gitlab.com/ee/ci/) have grown over time
into a sophisticated set of tools for describing and executing pipelines.
Taking advantage of these tools means learning and testing them in realistic
situations.

Projects themselves can serve as examples of CI/CD configuration.  However,
projects are not an ideal way of amassing knowledge about CI/CD techniques as
such, for several reasons:

- most projects have restricted access and can't always be shared for reference

- implementation may change; a project that serves as an illustrative example of
  a certain technique may not always use that technique
  
- projects involve many elements not related to CI/CD, which makes it harder to
  identify which parts are relevant

## Goals

This project aims to:

- help people learn Gitlab CI by working up from minimal examples

- illustrate and document common CI usages with focused, working examples

- support profiling of CI jobs and job features to inform performance optimization

- evolve best practices for common objectives

## Non-goals

This project does _not_ aim to:

- Maintain any resources used by actual projects. (For that, see
  https://git.mindgrub.net/web/ci-tools/) While the configurations should be
  tested and confirmed working, the code itself should be copied and adapted to
  specific projects where appropriate.

## Approach

The “by example” approach used here means that

- each example is focused on a specific topic

- each example is physically isolated and logically independent

  - the files for each example are contained in the example directory
  
  - examples don't know about each other
  
  
### Adding an example

To add an example:

- create a top-level directory in this project

- add a `README.md` to that directory documenting the example, including its
  purpose

- add a subsection to this `README` linking to it

- add example files in that directory, including at least one Gitlab CI
  configuration.

- add a manual job to the main [.gitlab-ci.yml](.gitlab-ci.yml) as follows

  ```yaml
  your_example:
    stage: examples
    when: manual
    trigger:
      include: your_example/.gitlab-ci.yml
  ```

Your `include` file does not have to be called `.gitlab-ci.yml`, but in general,
the directories are treated as if they are standalone projects.  That said,
file references in the example directories are relative to the project root, so
it's generally necessary to include the example directory in the path.

### Using environment variables

Some examples require environment variables to be set.  Environment variables
should be referenced only by one job, and should be named using a prefix
matching the example's directory name (but in ALL_CAPS).  For example, the
access token for the `npm_package_registry` example is called
`NPM_PACKAGE_REGISTRY_CI_TOKEN`.

## Use cases

Following are cases I'm interested in pursuing.

### Custom runner images

The [custom_runner](custom_runner/) example illustrates how to create a custom
build runner for speeding up jobs.

One of the techniques for reducing CI times is to package build dependencies
into a runner.  This is based on the observation that such dependencies change
far less often than the code that uses them.

The objective is to:
- build runner images
  - that are used by other jobs
  - and update when necessary (but not otherwise)
  
This technique is currently implemented in Thrive `web` and Adashi Broadcast,
but it doesn't yet work as intended.

### Conditional web builds

We have projects where API and web UI both live in the same repository.  This
arrangement is used where the API and web UI have a common dependency.  In
particular, the web UI may consume TypeScript types or other schema information
from a “core” package that is also used by the API.

However, in most cases, we are updating either the API or the web UI.  it should
be possible to design pipelines that detect what changed and build and deploy
only the necessary artifacts.

### JavaScript package registry

The [npm_package_registry](npm_package_registry/) example illustrates how to
publish an NPM package to the Gitlab project's package registry.

We are developing common “NPM” packages that can be consumed across multiple
projects.  Whether these packages are deployed to NPM itself or to Gitlab's
package registry, there are several CI-related considerations for the project
providing the package.

### CDK

CDK pipelines are just super slow.  Currently it appears that CloudFormation
itself is the worst offender in this area, but if there's anything we can do to
improve the performance (e.g. with finer job granularity), this project can be
used to explore those approaches.

See also https://git.mindgrub.net/gcannizzaro/typescript-cdk/

## Examples

This section walks through the examples, starting with the simplest and working
upward.

### Minimal

The [minimal example](/minimal/.gitlab-ci.yml) is small enough to include here
in its entirety:

```yaml
stages:
  - build

build_a:
  stage: build
  script:
    - echo 'Building ‘a’...'
```

This CI does not specify a runner and uses no files from the repository.  Its
only work is to echo one line of text.  As such, this example should provide a
lower bound on the possible runtime of a job in general.

The first two runs of this job (on 2020-12-18) took 12 and 11 seconds
respectively.  They were queued for a total of 24 seconds.

> 2 jobs for develop in 11 seconds (queued for 24 seconds) 

On a third run, it reports:

> 1 job for develop in 5 seconds (queued for 5 seconds)

### Node runner

Many web projects require NodeJS during the CI.  Increasingly, we use
custom-built runners for such jobs.  Those custom runners extend the [stock Node
images](https://hub.docker.com/_/node).

The [node_runner](./node_runner/.gitlab-ci.yml) example is identical to the
[minimal](#minimal) example, except that it calls for `node:12` as its base
image.  I picked Node 12 because
[“12.x”](https://aws.amazon.com/blogs/compute/node-js-12-x-runtime-now-available-in-aws-lambda/)
is the latest version available on AWS Lambda (as of 2020-12-18).  Running this
example can provide some basis for assessing the baseline cost of using a Node
image as against the runner's default.

> I don't know what the default image is.  I assume it's the cheapest option,
> but that could be wrong.


On the first run, this job reports (in the Gitlab UI):

> 1 job for develop in 6 seconds (queued for 2 seconds) 

However, I think that queued time is for the _job_, not the _pipeline_.
 
NOTE on pending time: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/21833

### TypeScript build

The [typescript_build](/typescript_build) example skips ahead a bit by adding
two things at once:

- installing TypeScript

- compiling with TypeScript

For this example, the code being compiled is so minimal that the job time will
be dominated by the installation time.  Also, the pipeline logs should indicate
how much time was used for each command.

A runtime report:

> 1 job for develop in 23 seconds (queued for 27 seconds) 

### Custom runner

The (custom_runner)[custom_runner] example... see (Custom runner
images)[#custom-runner-images].

-------------------------------------------------------------

## NOTE

**NOTE**: Remaining content is from my initial work learning Gitlab CI.  Some of
it is out of date.  I am moving to an approach using self-contained examples,
taking advantage of [Parent-child
pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html).

## simplest case

I started with a config like this:

```yaml
something:
  script:
    echo foo

something_else:
  script:
    # This isn't a known command so it should fail
    blah
```

I thought that the first task would succeed and the second one would fail, but
they both failed.  The logs for `something` look like this:

```
Running with gitlab-runner 10.1.0 (c1ecf97f)
  on kubernetes-runner (5f8a0024)
ERROR: Preparation failed: no image specified and no default set in config
Will be retried in 3s ...
ERROR: Preparation failed: no image specified and no default set in config
Will be retried in 3s ...
ERROR: Preparation failed: no image specified and no default set in config
Will be retried in 3s ...
ERROR: Job failed (system failure): no image specified and no default set in config
```

So you have to specify an image as the context in which the jobs will run.

The
“[Getting started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/README.html)”
docs don’t mention the word “image”.

Nor does the
“[Introduction to pipelines and jobs](https://docs.gitlab.com/ee/ci/pipelines.html)”.

Nor does
“[Configuring GitLab Runners](https://docs.gitlab.com/ee/ci/runners/README.html)”.

However it is mentioned in
“[image and services](https://docs.gitlab.com/ee/ci/yaml/README.html#image-and-services)”,

which is supposedly covered more fully in
“[Using Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)”,
under “[Docker integration](https://docs.gitlab.com/ee/ci/docker/README.html)”.


## when you specify an image

I don’t know where you specify a default image, so I added an explicit one to
the config:

```yaml
image: node:8

something:
  script:
    echo foo

something_else:
  script:
    # This isn't a known command so it should fail
    blah
```

Now the first job succeeds with

```
Running with gitlab-runner 10.1.0 (c1ecf97f)
  on kubernetes-runner (5f8a0024)
Using Kubernetes namespace: gitlab-jobs
Using Kubernetes executor with image node:8 ...
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-02vlxs to be running, status is Pending
Running on runner-5f8a0024-project-860-concurrent-02vlxs via gitlab-runner-3041082169-qbwxr...
Cloning repository...
Cloning into '/gcannizzaro/ci-tests'...
Checking out 6711aaeb as master...
Skipping Git submodules setup
$ echo foo
foo
Job succeeded
```

while the second job fails with

```
Running with gitlab-runner 10.1.0 (c1ecf97f)
  on kubernetes-runner (5f8a0024)
Using Kubernetes namespace: gitlab-jobs
Using Kubernetes executor with image node:8 ...
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-1v0gtm to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-1v0gtm to be running, status is Pending
Waiting for pod gitlab-jobs/runner-5f8a0024-project-860-concurrent-1v0gtm to be running, status is Pending
Running on runner-5f8a0024-project-860-concurrent-1v0gtm via gitlab-runner-3041082169-qbwxr...
Cloning into '/gcannizzaro/ci-tests'...
Cloning repository...
Checking out 6711aaeb as master...
Skipping Git submodules setup
$ blah
/bin/bash: line 49: blah: command not found
ERROR: Job failed: error executing remote command: command terminated with non-zero exit code: Error executing in Docker Container: 1
```

From here on, I'll omit the part of the log up to the first script.


## installing node production dependencies

Now let’s suppose you have the following `package.json`:

```json
{
  "dependencies": {
    "js-csp": "^1.0.1"
  }
}
```

And the following `.gitlab-ci.yml`:

```yaml
image: node:8

install_dependencies:
  script:
    npm install
```

This succeeds, and the job log says:

```
$ npm install
npm WARN ci-tests No repository field.
npm WARN ci-tests No license field.

added 5 packages in 0.995s
Job succeeded
```

## using node production dependencies

Once the dependencies are installed, they should be available to other commands
in the runner:

```yaml
image: node:8

install_dependencies:
  script:
    - npm install
    - node use-csp
```

**N.B.** - The `script` is a *list* of commands.  If you leave out the leading `-`’s, the
lines will be interpreted as the single command `npm install node use-csp`, and
the runner will complain that `npm ERR! 404 Not Found: use-csp@latest`.

And `use-csp.js` is:

```javascript
const csp = require("js-csp");

const important_events = csp.chan(10);
// Alice is watching
csp.go(function * () {
	const thing = yield csp.take(important_events);
	console.log("Alice saw a " + thing);
});
// Bob is watching
csp.go(function * () {
	const thing = yield csp.take(important_events);
	console.log("Bob saw a " + thing);
});
csp.putAsync(important_events, "solar eclipse");
```

This succeeds with the job log:

```
$ npm install
npm WARN ci-tests No repository field.
npm WARN ci-tests No license field.

added 5 packages in 1.002s
$ node use-csp
Alice saw a solar eclipse
Job succeeded
```

## installing node production dependencies

For builds, you will usually need dev dependencies.  If you have this
`package.json`:

```json
{
  "dependencies": {
    "js-csp": "^1.0.1"
  },
  "devDependencies": {
    "typescript": "^2.6.1"
  },
  "scripts": {
    "build": "tsc use-typescript"
  }
}
```

and this CI config:

```yaml
image: node:8

install_dependencies:
  script:
    - npm install
    - npm run build
    - node use-typescript
```

And `use-typescript.ts` is:

```typescript
function log(name: string, thing: any): void {
	console.log(`${name} saw a ${thing}`);
}

log("Alice", "UFO");
log("Bob", "sight");
```

This succeeds and logs:

```
$ npm install
npm WARN ci-tests No repository field.
npm WARN ci-tests No license field.

added 6 packages in 1.852s
$ npm run build

> @ build /gcannizzaro/ci-tests
> tsc use-typescript

$ node use-typescript
Alice saw a UFO
Bob saw a sight
Job succeeded
```

**N.B.**: So note that it is (apparently) not necessary to specify a development
`NODE_ENV` or instruct `npm install` to include `devDependencies`, as this happens by
default.


## adding a commit in the build

If you want to bump a package's version number as part of its CI process, you'll
need to modify the repository for which the pipeline is running.

[npm version](https://docs.npmjs.com/cli/version) “If run in a git repo...; will
also create a version commit and tag.”

Can you do this from GitLab CI?  Create a commit and push back to the origin?

```yaml
image: node:8

install_dependencies:
  script:
    - npm install
    - npm run build
    - node use-typescript
    # Build okay, now eliminate build artifacts
    - git reset --hard HEAD
    # Get out of deatched head state
    - git checkout -b ci_processing
    # Commit requires config and this environment does not have your config
    - git config --global user.name "GitLab CI"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - npm version patch -m "Version %s [skip ci]"
    # Push back to same repo
    - git remote set-url --push origin "${CI_REPOSITORY_URL}"
    - git push https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git ci_processing:${CI_BUILD_REF_NAME}
```

This probably won’t work because you need a token to commit.

Note that before you do `npm version patch`, you have to have a clean working
tree, or you get:

```
npm ERR! Git working directory not clean.
```

Resetting isn’t really what you want if you’re building a distributable.  But
one thing at a time.

Also, git won’t let you commit without a username, etc, and this applies to `npm
version patch` as well:

```
npm ERR! code 128
npm ERR! Command failed: git commit -m Version 0.0.1 [skip ci]
npm ERR! 
npm ERR! *** Please tell me who you are.
npm ERR! 
npm ERR! Run
npm ERR! 
npm ERR!   git config --global user.email "you@example.com"
npm ERR!   git config --global user.name "Your Name"
npm ERR! 
npm ERR! to set your account's default identity.
npm ERR! Omit --global to set the identity only in this repository.
```

With that, `npm version patch` works, but you still can’t push:

```
$ git push origin
warning: push.default is unset; its implicit value has changed in
[... git warning about matching mode...]
fatal: You are not currently on a branch.
To push the history leading to the current (detached HEAD)
state now, use

    git push origin HEAD:<name-of-remote-branch>

ERROR: Job failed: error executing remote command: command terminated with non-zero exit code: Error executing in Docker Container: 1
```

But if you want to push a branch (which is questionable, since it’s a
workaround), you have to do `--set-upstream` (which I think is because of the way
GitLab is configured).

```
fatal: The current branch ci_processing has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin ci_processing

ERROR: Job failed: error executing remote command: command terminated with
-zero exit code: Error executing in Docker Container: 1
```

With that, you’ll run into this problem:

```
$ git push --set-upstream origin ci_processing
remote: You are not allowed to upload code for this project.
fatal: unable to access 'https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@git.mindgrub.net/gcannizzaro/ci-tests.git/': The requested URL returned error: 403
ERROR: Job failed: error executing remote command: command terminated with non-zero exit code: Error executing in Docker Container: 1
```

But you don’t really want to create a branch, anyway.

You can solve that by

-   creating a personal access token
-   adding it to the project secrets
-   referencing in the push, as done in the example.

```sh
git push
https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git
ci_processing:${CI_BUILD_REF_NAME}
```

I’d like to not hardcode the repo name, which I suppose I can do by parsing
`CI_REPOSITORY_URL`
([formerly](https://docs.gitlab.com/ee/ci/variables/#9-0-renaming)
`CI_BUILD_REPO`) as
done [here](https://gitlab.com/gitlab-org/gitlab-ce/issues/18106#note_12209180).


## create a pipeline that fails

You have the option to
[Only allow merge requests to be merged if the pipeline succeeds](https://docs.gitlab.com/ce/user/project/merge_requests/merge_when_pipeline_succeeds.html#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds).
Used in conjunction with
[protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html),
you can ensure that a certain branch does not receive broken builds (provided
you don’t push them yourself!)

The following pipeline will fail before trying the `install_dependencies` task:

```yaml
image: node:8

fail_early:
  script:
    boo
    
install_dependencies:
  stage: deploy
  variables:
    CI_DEBUG_TRACE: "true"
  script:
    - npm install
    - npm run build
```

**Note** that this pipeline will **not** prevent merges, even though the pipeline
does fail:

```yaml
image: node:8

fail_early:
  script:
    boo
    
publish:
  variables:
    CI_DEBUG_TRACE: "true"
  script:
    # Get out of deatched head state
    - git checkout -b ci_processing
    # Commit requires config and this environment does not have your config
    - git config --global user.name "GitLab CI"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - npm version patch -m "Version %s [skip ci]"
    # Push back to same repo
    - git remote set-url --push origin "${CI_REPOSITORY_URL}"
    - git push https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git ci_processing:${CI_BUILD_REF_NAME}
```

Why?  Because the `publish` job always runs, and always creates a new commit
which specifies `[skip ci]`.  As a result, the most recent commit on the branch
does not have a failed status so GitLab does not flag it as unmergable.

Specifying `stage: deploy` (or any other stage that follows the failed jobs)
prevents this from running in the first place and gives the intended result.


## run a job only on a specific branch

Use the `only` field to limit a job to a specific branch:

```yaml
image: node:8

install_dependencies:
  only:
    - public
  stage: deploy
  variables:
    CI_DEBUG_TRACE: "true"
  script:
    - npm install
    - npm run build
    - node use-typescript
    # Build okay, now eliminate build artifacts
    - git reset --hard HEAD
    # Get out of deatched head state
    - git checkout -b ci_processing
    # Commit requires config and this environment does not have your config
    - git config --global user.name "GitLab CI"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - npm version patch -m "Version %s [skip ci]"
    # Push back to same repo
    - git remote set-url --push origin "${CI_REPOSITORY_URL}"
    - git push https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git ci_processing:${CI_BUILD_REF_NAME}
```


## tag a commit

`npm version` tags new commits that it creates.  However, since the CI runs in a
temporary environment, the tags will be lost unless you push them explicitly.

```sh
git push --follow-tags # etc
```


## multiple jobs that re-use installed packages

This example introduces several features:

```yaml
image: node:8

before_script:
  - npm install

cache:
  paths:
    - node_modules/

build:
  stage: build
  artifacts:
    paths:
      - dist
  script:
    - npm run build
    
test:
  stage: test
  script:
    - node use-typescript
  
mark_release:
  only:
    - public
  stage: deploy
  variables:
    CI_DEBUG_TRACE: "true"
  script:
    # Build okay, now eliminate build artifacts
    - git reset --hard HEAD
    # Get out of deatched head state
    - git checkout -b ci_processing
    # Commit requires config and this environment does not have your config
    - git config --global user.name "GitLab CI"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - npm version patch -m "Version %s [skip ci]"
    # Push back to same repo
    - git remote set-url --push origin "${CI_REPOSITORY_URL}"
    - git push --follow-tags https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git ci_processing:${CI_BUILD_REF_NAME}
```

This adds

-   `build` and `test` jobs that will run on all branches.
-   a `before_script` that runs `npm install` before all jobs
-   `cache` the `node_modules` directory between jobs and pipelines
-   `artifacts` from the build job, including the distributables.

I’m not sure that it’s necessary to specify the `cache`.  The docs
about [cache:key](https://docs.gitlab.com/ee/ci/yaml/README.html#cache-key) say:

> The default key is **default** across the project, therefore everything is shared
> between each pipelines and jobs by default, starting from GitLab 9.0.

-   Don’t: use `untracked: true` for `artifacts`
-   Do: specify `artifacts`.  If you include everything untracked, this will include
    your `node_modules`, which may be large and shouldn’t be included with, e.g. the
    download artifacts.

As the docs for [dependencies](https://docs.gitlab.com/ee/ci/yaml/README.html#dependencies) note:

> Note that artifacts from all previous stages are passed by default.


## npm publish

`npm login` does not accept username and password parameters,
and
[“this absence is by design.”](https://github.com/npm/npm/issues/12111#issuecomment-202497588) And
since your AD credentials are your login for `npm.mindgrub.net`, you wouldn’t
want to put them in the project secrets, anyway.

The solution is to use an existing access token.  Once you’ve done `npm login`
from your machine, an access token is added to your `~/.npmrc` file, which you
can use as described in
[this article](https://remysharp.com/2015/10/26/using-travis-with-private-npm-deps).

The following job successfully publishes to `npm.mindgrub.net` after the build:

```yaml
publish:
  only:
    - public
  stage: deploy
  variables:
    CI_DEBUG_TRACE: "true"
  script:
    # Get out of detached head state
    - git checkout -b temp_publish
    
    # Commit requires config and this environment does not have your config
    
    - git config --global user.name "GitLab CI"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    
    # Bump version number and use commit message
    - npm version patch --force  -m "v%s [skip ci] `git log -n1 --format='%s'`"
    
    # Publish
    - echo "registry=https://npm.mindgrub.net" > .npmrc
    - echo "//npm.mindgrub.net/:_authToken=\"\${NPM_AUTH_TOKEN}\"" >> .npmrc
    - npm publish
    
    # If publish succeeds, push the version bump
    - git remote set-url --push origin "${CI_REPOSITORY_URL}"
    - git push --follow-tags https://ci-tests:${PERSONAL_ACCESS_TOKEN}@git.mindgrub.net/gcannizzaro/ci-tests.git temp_publish:${CI_BUILD_REF_NAME}
```

Where you add `NPM_AUTH_TOKEN` to the project secrets.

You also need to write to `.npmrc` to set the registry, because [\`npm config set\`
always modifies ~/.npmrc](https://github.com/npm/npm/issues/7771).


## push version number back to source branch

When you bump the version number on the `public` branch, that commit needs to
get back upstream to the branch that people work from.

## "deploy" to an environment

Adding the following will cause a stub job to run on all successful pushes:

```yaml
deploy_dev:
  stage: deploy
  environment:
    name: imaginary-dev
  script:
    - echo "mock deployment"
```


## deploy to Deis

TBD


## create a CI configuration that runs build tests


## create a CI configuration that updates another repository with build artifacts


## create a CI configuration that uses environment variables from settings


## create a CI configuration that uses secret environment variables from settings


## create a CI configuration that runs different targets based on which branch is updated


## create a CI configuration that launches or restarts a Docker container somewhere


## create a CI configuration that pushes to EC2

